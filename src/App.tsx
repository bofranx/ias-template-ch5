import React, { useState } from 'react';
import './tailwind.output.css';
import './App.css';
import VolumeBar from './components/VolumeBar/VolumeBar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faShare, faPhone, faCogs, faHome } from '@fortawesome/free-solid-svg-icons'
import Settings from './components/Settings/Settings';
import Dialer from './components/Dialer/Dialer';
import Header from './components/Header/Header';
import Main from './components/Main/Main';

function App() {

  library.add(faShare, faPhone, faCogs, faHome);

  const [nav, setNav] = useState("none")

  return (
    <div className=" w-full h-screen bg-gray-200 overflow-hidden box-border">

      <div className="relative w-full h-full flex flex-col items-center justify-between">
        <div className="w-full h-auto ">
          <Header title="Conference Room" subtitle="IAS Technology" height="4.5rem">
            <VolumeBar volumeUpSignalName="1" volumeDownSignalName="2" volumeMuteSignalName="3" volumeBarSignalName="1" volumeBarBottom={true} alwaysShowVolume={false} />

          </Header>
        </div>
        <div className="relative w-full h-full flex">


          <Main isVisible={nav === 'none'}>
            <div className="flex flex-col justify-center items-center m-4">
              <div className="cursor-pointer rounded-full shadow flex items-center justify-center bg-blue-400 h-48 w-48 m-4 text-gray-100 text-5xl">
                <FontAwesomeIcon icon="share" />
              </div>
              <p className="text-gray-700 text-2xl tracking-wide">Share</p>
            </div>

            <div className="flex flex-col justify-center items-center m-4 ">
              <div
                className="cursor-pointer rounded-full shadow flex justify-center items-center bg-green-400 h-48 w-48 m-4 text-gray-100 text-5xl"
                onClick={() => {
                  setNav('dialer')
                }}
              >
                <FontAwesomeIcon icon="phone" />
              </div>
              <p className="text-gray-700 text-2xl tracking-wide">Call</p>
            </div>

            <div className="flex flex-col justify-center items-center  m-4">
              <div
                className="cursor-pointer rounded-full shadow flex justify-center items-center bg-gray-400 h-48 w-48 m-4 text-gray-100 text-5xl"
                onClick={() => {
                  setNav("settings");
                }}
              >
                <FontAwesomeIcon icon="cogs" />

              </div>
              <p className="text-gray-700 text-2xl tracking-wide">Settings</p>
            </div>
          </Main>


          <Dialer onClose={() => setNav('none')} isVisible={nav === "dialer"} />
          <Settings isVisible={nav === "settings"} >
            this is the settings page
          </Settings>

        </div>


        <div className="flex  justify-center items-center w-full ">
          <div
            className={`cursor-pointer absolute flex justify-center items-center bottom-0 text-gray-700 text-xl mb-4 transform transition-all duration-700 ${nav === "none" ? "translate-y-64 opacity-0" : "translate-y-0 opacity-100"}`}
            onClick={() => setNav("none")}
          >
            <FontAwesomeIcon icon={faHome} className="text-5xl" />
          </div>
        </div>


      </div>


    </div >
  );
}

export default App;
