import React, {useEffect} from 'react'
import './TVControlsModal.scss'
import close from '../../assets/close_lg.png'
import logo from '../../assets/IAS technology color.png'

type TVControlsModalProps = {
    title?: string
    subTitle?: string
    isActive: boolean
    onClick: () => void
}

const TVControlsModal: React.FunctionComponent<TVControlsModalProps> = (props) => {
       
    const startTimeout = () => {
        timeout = setTimeout(function () {
            props.onClick();
        }, 30000);
    }

    const stopTimeout = () =>{
    clearTimeout(timeout);
    }
        
    var timeout: NodeJS.Timeout;


    useEffect(() => {
        startTimeout(); 
        return () => {
            stopTimeout();
        }
    })
    

    return (
        <div
        className={`modal ${props.isActive ? "active" : "hidden"}`}
        >
        
            

            <div className="container">
                <div className="title"><h2>{props.title || ""}</h2>
                    <h5>{props.subTitle || ""}</h5></div>
                <div className="header">
                    
                    <div className="close" onClick={() => {
                        props.onClick();
                        stopTimeout();
                    }}>
                        <img src={close} alt="close"/>
                    </div>

                </div>

                   
              
                <div className="content">
                   
               </div>
                <div className="footer">
                    Created by
                     <img src={logo} alt="IAS logo"/>
                </div>
            </div>

           


            
        </div>
        
    )
}

export default TVControlsModal;
