import React from 'react'

type MainProps = {
    isVisible?: boolean
}

const Main: React.FunctionComponent<MainProps> = (props) => {

    return (

        < div className={`absolute w-full h-full ${props.isVisible ? "scale-100 opacity-100 visible " : "scale-0 opacity-0 invisible"} transform transition-all duration-300 flex items-center justify-center`}>
            <div className="flex flex-row justify-center items-center">
                {props.children}
            </div>
        </div>
    )
}

export default Main
