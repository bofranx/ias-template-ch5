import React from 'react'
import './Display.scss'

type DisplayProps = {
    label?: string
    subLabel?: string
}

const Display: React.FunctionComponent<DisplayProps> = (props) => {

    return (
        <div
            className="display-container"            
        >
            {props.label && 
                <div className="text-container">
                <h2>
                {props.label}
                </h2>
                </div>
                
                }
            
            {props.subLabel &&
                <div className="text-container">
                <h5>
                {props.subLabel}
                </h5>
                </div>
                }
     
            <div
                className="display-children">
                    {props.children}
            </div>
        </div>
    )
}

export default Display;