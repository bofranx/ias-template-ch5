import React from 'react'
import './Button.scss'
import { usePublishDigital, useSubscribeDigital } from 'react-ch5'

type ButtonProps = {
    publishSignalName: string,
    subscribeSignalName: string,
    className?: string,
    styleOnClassName?: string,
    styleOffClassName?: string
    onClick?: () => void
}

const Button: React.FunctionComponent<ButtonProps> = (props) => {

    const press = usePublishDigital(props.publishSignalName);
    const feedback = useSubscribeDigital(props.subscribeSignalName);

    return (
        <div 
            className={`${props.className || "btn"} ${feedback ? (props.styleOnClassName || "on") : (props.styleOffClassName || "off")}`}
            onTouchStart={()=>{press(true)}}
            onTouchEnd={() => { press(false) }}
            onTouchCancel={() => { press(false) }}
            onClick={props.onClick}
            >
            {props.children}
        </div>
    )
}

export default Button;