import React, { useEffect, useLayoutEffect, useRef, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faVolumeDown, faVolumeUp, faVolumeMute, faVolumeOff } from '@fortawesome/free-solid-svg-icons'
import { usePublishDigital, useSubscribeAnalog, useSubscribeDigital } from 'react-ch5';
import './VolumeBar.css'

type VolumeBarTypes = {
    volumeUpSignalName: string,
    volumeDownSignalName: string,
    volumeMuteSignalName: string,
    volumeBarSignalName: string,
    containerHeight?: number,
    volumeBarTop?: boolean,
    volumeBarBottom?: boolean,
    alwaysShowVolume?: boolean
}

const VolumeBar: React.FunctionComponent<VolumeBarTypes> = (props) => {

    library.add(faVolumeUp, faVolumeDown, faVolumeMute, faVolumeOff)

    const upFeedback = useSubscribeDigital(props.volumeUpSignalName)
    const downFeedback = useSubscribeDigital(props.volumeDownSignalName)
    const muteFeedback = useSubscribeDigital(props.volumeMuteSignalName)
    const volumeFeedback = useSubscribeAnalog(props.volumeBarSignalName)

    const upPublish = usePublishDigital(props.volumeUpSignalName)
    const downPublish = usePublishDigital(props.volumeDownSignalName)
    const mutePublish = usePublishDigital(props.volumeMuteSignalName)

    const [volumeBarWidth, setVolumeBarWidth] = useState<number>(0);
    const [volumeBarVisible, setVolumeBarVisible] = useState<boolean>(false);
    const [containerHeight, setContainerHeight] = useState<number>();


    const containerRef = useRef<HTMLDivElement>(null);



    useLayoutEffect(() => {
        setContainerHeight(containerRef.current?.getBoundingClientRect().height)
    }, [containerRef])


    useEffect(() => {
        setVolumeBarWidth((volumeFeedback * 100) / 65535);

        let volumeBarTimer: NodeJS.Timeout;

        function showVolumeTimeout() {
            clearTimeout(volumeBarTimer);
            setVolumeBarVisible(true);
            volumeBarTimer = setTimeout(function () {
                setVolumeBarVisible(false);
            }, 3000);
        }

        showVolumeTimeout();

        return () => {
            clearTimeout(volumeBarTimer)
        }
    }, [volumeFeedback])


    return (
        < div
            ref={containerRef}
            className={`flex flex-row justify-between items-center relative h-full ${containerHeight as number > 99 ? "text-4xl" : "text-3xl"} text-gray-700`}
            style={{
                height: props.containerHeight
            }}
        >

            <div className={`flex justify-center items-center rounded-full ${muteFeedback ? "bg-red-500 text-gray-200" : "bg-gray-400"} m-2 cursor-pointer outline-none`}
                style={{
                    height: containerHeight,
                    width: containerHeight
                }}
                onTouchStart={() => {
                    mutePublish(true);
                }}
                onTouchEnd={() => {
                    mutePublish(false)
                }}
            >
                <FontAwesomeIcon icon="volume-mute" />
            </div>
            <div className={`flex justify-center items-center rounded-full ${downFeedback ? "bg-blue-400" : "bg-gray-400"}  m-2 cursor-pointer outline-none hover:bg-gray-500`}
                style={{
                    height: containerHeight,
                    width: containerHeight
                }}
                onTouchStart={() => {
                    downPublish(true);
                }}
                onTouchEnd={() => {
                    downPublish(false);
                }}
            >
                <FontAwesomeIcon icon="volume-down" />
            </div>
            <div className={`flex justify-center items-center rounded-full ${upFeedback ? "bg-blue-400" : "bg-gray-400"}  m-2 cursor-pointer outline-none hover:bg-gray-500`}
                style={{
                    height: containerHeight,
                    width: containerHeight
                }}
                onTouchStart={() => {
                    upPublish(true);
                }}
                onTouchEnd={() => {
                    upPublish(false);
                }}
            >
                <FontAwesomeIcon icon="volume-up" />
            </div>

            <div className={`overflow-hidden absolute ${containerHeight as number > 99 ? "h-2" : "h-1"} w-full rounded bg-gray-700 transform ${props.volumeBarTop ? "top-0 -translate-y-4" : props.volumeBarBottom ? "bottom-0 translate-y-4" : "top-0 -translate-y-4"} transition-opacity duration-150 ${props.alwaysShowVolume ? "opacity-100" : volumeBarVisible ? "opacity-100" : "opacity-0"}`} >
                <div className={`bg-blue-400 h-full transition-all duration-150`} style={{ width: volumeBarWidth + "%" }}>

                </div>
            </div>

        </div >
    )
}

export default VolumeBar;
