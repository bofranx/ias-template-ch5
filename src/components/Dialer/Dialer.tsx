import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone, faPhoneAlt, faLaptop, faEllipsisH, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { faUsb } from '@fortawesome/free-brands-svg-icons';

type DialerProps = {
    isVisible: boolean;
    onClose: () => void;
}

const Dialer: React.FunctionComponent<DialerProps> = (props) => {

    const [callOption, setCallOption] = useState<string>("none");

    const keypad = [
        {
            number: "1",
            label: ""
        },
        {
            number: "2",
            label: "ABC"
        },
        {
            number: "3",
            label: "DEF"
        },
        {
            number: "4",
            label: "GHI"
        },
        {
            number: "5",
            label: "JKL"
        },
        {
            number: "6",
            label: "MNO"
        },
        {
            number: "7",
            label: "PQRS"
        },
        {
            number: "8",
            label: "TUV"
        },
        {
            number: "9",
            label: "WXYZ"
        },
        {
            number: "*",
            label: ""
        },
        {
            number: "0",
            label: "+"
        },
        {
            number: "#",
            label: ""
        }
    ]

    return (

        <div className={` absolute ${props.isVisible ? "opacity-100 visible" : "opacity-0 invisible"} transform transition-all duration-300 h-full w-full`}>

            {callOption != "none" &&
                <div className="flex justify-center items-center">
                    <div
                        className="cursor-pointer fixed top-0 mx-auto w-auto flex flex-row justify-center items-center mt-2 transform -translate-y-16"
                        onClick={() => setCallOption("none")}
                    >
                        <FontAwesomeIcon icon={faChevronLeft} className="text-xl text-gray-700 mr-4"
                        />
                        <h1
                            className="text-gray-700 text-md font-semibold">
                            Back to call options
                        </h1>
                    </div>
                </div>
            }


            <div className="relative w-full flex items-center justify-center h-full overflow-hidden">
                <div className={`absolute w-full flex flex-col items-center justify-center mb-16 transition-all duration-300 ${callOption === "none" && props.isVisible ? "opacity-100 visible" : "opacity-0 invisible"}`}>

                    <h1 className="w-full mx-auto text-center text-gray-700 font-light text-3xl mb-8">
                        Select a call mode
                        </h1>
                    <div className="flex flex-row items-center justify-center text-gray-200">
                        <div className="flex flex-col justify-center items-center">
                            <div
                                className="cursor-pointer mx-16 w-24 h-24 rounded-full bg-green-400 flex justify-center items-center shadow-sm"
                                onClick={() => setCallOption("phone")}
                            >
                                <FontAwesomeIcon icon={faPhone} className="text-3xl" />

                            </div>
                            <p className="text-lg text-gray-600 mt-4">
                                Phone Call
                                </p>
                        </div>

                        <div className="flex flex-col justify-center items-center">

                            <div
                                className="cursor-pointer mx-16 w-24 h-24 rounded-full bg-purple-400 flex justify-center items-center shadow-sm"
                                onClick={() => setCallOption("usb")}
                            >
                                <FontAwesomeIcon icon={faUsb} className="text-3xl" />

                            </div>
                            <p className="text-lg text-gray-600 mt-4">
                                Web Conference
                                </p>
                        </div>
                    </div>

                </div>



                <div className={`relative w-full h-full flex items-center justify-center transition-all duration-300 ${callOption === "phone" && props.isVisible ? "opacity-100 visible" : "opacity-0 invisible"}`}>

                    {/* keypad */}
                    <div className=" mx-auto grid grid-cols-3 gap-4">
                        {keypad.map((key, index) =>
                            <div key={key.number} className="cursor-pointer relative flex items-center justify-center pb-2 text-3xl text-gray-700 bg-gray-400 rounded-full hover:bg-opacity-50 transition-all duration-150 h-24 w-24">
                                {key.number}
                                <div className="absolute bottom-0 mb-2 text-sm tracking-widest text-gray-600">
                                    {key.label}
                                </div>
                            </div>
                        )}


                    </div>

                </div>


                <div className={`absolute w-full h-full flex items-center justify-center transition-all duration-300 ${callOption === "usb" && props.isVisible ? "opacity-100 visible" : "opacity-0 invisible"}`}>

                    <div className="relative mx-auto w-1/2 h-full flex flex-col  items-center">

                        <h1 className="w-full h-auto mx-auto text-center text-gray-700 font-light text-3xl mb-16 ">
                            To call using <span className="text-purple-700 font-normal">Microsoft Teams</span>:
                        </h1>
                        <div>
                            <div className="w-full flex flex-col items-center justify-center mb-6 ">
                                <div className=" text-orange-500 text-3xl m-0 flex justify-center items-center">
                                    Step <span className="font-bold ml-2">1</span>
                                </div>
                                <div className="w-full relative flex flex-row justify-start  h-1/6 mt-2">
                                    <div className="w-1/6">
                                        <FontAwesomeIcon icon={faUsb} className="text-orange-500 text-3xl mr-2" />
                                    </div>

                                    <h2 className="w-full text-xl text-gray-700">
                                        Connect a device using the USB cable provided
                                </h2>
                                </div>
                            </div>

                            <div className="w-full flex flex-col items-center justify-center mb-6">
                                <div className=" text-blue-500 text-3xl m-0 flex justify-center items-center">
                                    Step <span className="font-bold ml-2">2</span>
                                </div>
                                <div className="w-full relative flex flex-row justify-start  h-1/6 mt-2">
                                    <div className="w-1/6">
                                        <FontAwesomeIcon icon={faLaptop} className="text-blue-500 text-3xl mr-2 " />
                                    </div>
                                    <h2 className="w-full text-xl text-gray-700">
                                        Display video from your device using the cables provided
                                </h2>
                                </div>
                            </div>

                            <div className="w-full flex flex-col items-center justify-center mb-6">
                                <div className=" text-green-500 text-3xl m-0 flex justify-center items-center">
                                    Step <span className="font-bold ml-2">3</span>
                                </div>
                                <div className="w-full relative flex flex-row justify-start h-1/6 mt-2">
                                    <div className="w-1/6">
                                        <FontAwesomeIcon icon={faEllipsisH} className="text-green-500 text-3xl mr-2 " />
                                    </div>
                                    <h2 className="w-full text-xl text-gray-700">
                                        Change the audio and video device in your software to use the room's device
                                </h2>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>



            </div>
        </div >



    )
}

export default Dialer;

