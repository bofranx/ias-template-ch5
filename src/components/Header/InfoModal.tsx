import React, { useEffect, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes, faPowerOff } from '@fortawesome/free-solid-svg-icons'

type InfoModalProps = {
    className?: string,
    isVisible: boolean,
    onClick: () => void,
    title?: string,
    subtitle?: string
}

export const InfoModal: React.FunctionComponent<InfoModalProps> = (props) => {

    const [sidePanel, setSidePanel] = useState(false)

    useEffect(() => {

        if (props.isVisible) {
            setSidePanel(true)
        }
        else {
            setSidePanel(false)
        }

    }, [props.isVisible])

    return (
        <div className={`absolute ${props.isVisible ? "opacity-100 visible" : "opacity-0 invisible"} transform transition-all duration-300 w-screen h-screen ${props.className}  bg-black bg-opacity-75`}>
            <div
                className="relative w-full h-full"
                onClick={props.onClick}
            >
            </div>
            <div
                className={`absolute top-0 left-0 h-full bg-gray-100 transform transition-all duration-300 ${sidePanel ? "w-1/4" : "w-0"}`}
            >
                <div
                    className={`h-full flex flex-col ${sidePanel ? "opacity-100 duration-1000" : "opacity-0 duration-150 "}`

                    }>

                    <section id="header">
                        <div className="flex flex-row justify-between items-center h-auto mt-2 ml-4">
                            <div className={`w-fullflex flex-col `}>
                                <h1 className="text-gray-700 text-2xl font-medium">{props.title}</h1>
                                <h3 className="text-gray-600 text-md">{props.subtitle}</h3>
                            </div>

                            <div
                                onClick={props.onClick}
                                className="h-auto w-auto p-2 mr-2 flex justify-end items-center cursor-pointer">
                                <FontAwesomeIcon icon={faTimes} className="text-4xl text-gray-700" />
                            </div>
                        </div>
                    </section>

                    <section id="menu">
                        <div className="w-full h-full flex flex-col  items-center mt-16 list-none">
                            <ul className="w-5/6">
                                <li className="h-16 mb-4  border-b border-gray-700 border-opacity-25 flex items-center">
                                    Settings Menu Item
                            </li>
                                <li className="h-16 mb-4  border-b border-gray-700 border-opacity-25 flex items-center">
                                    Settings Menu Item
                            </li>
                                <li className="h-16 mb-4  border-b border-gray-700 border-opacity-25 flex items-center">
                                    Settings Menu Item
                            </li>
                                <li className="h-16 mb-4  border-b border-gray-700 border-opacity-25 flex items-center">
                                    Settings Menu Item
                            </li>
                                <li className="h-16 mb-4  border-b border-gray-700 border-opacity-25 flex items-center">
                                    Settings Menu Item
                            </li>

                            </ul>
                        </div>
                    </section>

                    <div className=" mt-auto h-auto w-full flex items-center justify-center mb-4">
                        <div className="w-1/2 flex items-center justify-center rounded h-full bg-red-500 p-5 text-gray-200">
                            <FontAwesomeIcon icon={faPowerOff} className="mr-2 text-xl" />
                            <h1 className="text-xl">Shutdown</h1>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    )
}
