import React, { useLayoutEffect, useRef, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { InfoModal } from './InfoModal';

type HeaderProps = {
    height?: string,
    title?: string,
    subtitle?: string
}

const Header: React.FunctionComponent<HeaderProps> = (props) => {

    const [containerHeight, setContainerHeight] = useState<number>();
    const [infoPanel, setInfoPanel] = useState(false);
    const containerRef = useRef<HTMLDivElement>(null);

    useLayoutEffect(() => {
        setContainerHeight(containerRef.current?.getBoundingClientRect().height)
    }, [containerRef])

    return (
        <div
            className="w-full h-full z-40"
            style={{
                height: props.height
            }}
        >
            <InfoModal
                isVisible={infoPanel}
                onClick={() => setInfoPanel(false)}
                className="z-50"
                title={props.title}
                subtitle={props.subtitle}
            />
            <div ref={containerRef} className="w-full h-full mt-0 pt-0">



                <div className="flex flex-row w-full h-full justify-between items-center">
                    <div className="flex flex-row justify-center items-center w-auto h-auto pt-4 ">
                        <div
                            className=" cursor-pointer rounded-full text-gray-700 text-3xl flex items-center justify-center"
                            style={{
                                height: containerHeight,
                                width: containerHeight
                            }}
                            onClick={() => setInfoPanel(!infoPanel)}
                        >
                            <FontAwesomeIcon icon={faBars} />
                        </div>
                        <div className="flex flex-col place-items-start">
                            <h1 className="top-0 font-medium text-gray-700 text-2xl ">
                                {props.title}
                            </h1>
                            <h3 className="text-gray-600 text-md ">
                                {props.subtitle}
                            </h3>
                        </div>
                    </div>
                    <div className="mr-2 relative h-full mt-6">
                        {props.children}
                    </div>
                </div>
            </div>

        </div >
    )
}

export default Header
