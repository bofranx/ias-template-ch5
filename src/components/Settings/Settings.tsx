import React from 'react'

type SettingsProps = {
    isVisible: boolean,
    onClose?: () => void,
    label?: string,
    subLabel?: string
}

const Settings: React.FunctionComponent<SettingsProps> = (props) => {

    return (
        <div className={` absolute ${props.isVisible ? "opacity-100 visible" : "opacity-0 invisible"} transform transition-all duration-300 h-full w-full`}>
            <div className="w-full h-full flex flex-col items-center justify-center">
                {props.children}
            </div>
        </div>
    )
}

export default Settings;